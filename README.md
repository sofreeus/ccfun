# Cloud Computing Fundamentals

A full-day, live, in-person and online, intensely hands-on, expert-led introduction to cloud computing (currently heavily AWS-flavored, sorry)

see also:
- [Intro to DevOps](https://gitlab.com/sofreeus/introtodevops)
- [A Gentle, Hands-on Intro to OpenStack](https://gitlab.com/sofreeus/AGHI2OpenStack)

## Units

## 00. [Context, Setup, and Introduction](/units/00_A_Gentle_Introduction/README.md)

build up, deploy, tear down

### 01. [Users and Keys](/units/01_user_and_key/README.md)

- Create IAM and Linux user accounts
- Create key

### 02. [Instance and Website](/units/02_instance_and_website/README.md)

- Create instance
- Connect instance to DNS
- Configure instance into a webserver

### 03. [Reliable DB](/units/03_db/README.md)

- Create RDS cluster
- Copy DB
- Reconfigure instance
- Test app && Delete local DB

### 04. [Load Balancer](/units/04_lb/README.md)

- Create load-balancer

### 05. [AutoScaling Group](/units/05_asg/README.md)

- Create autoscaling group for your web app

Test app && terminate member instance (a load-test would be nice-to-have)

### 99. Tear down

- Delete RDS cluster
- Delete autoscaling group
- Delete DNS record
- Delete launch template
- Delete image
- Delete snapshots
- Delete Load Balancer
- Delete Target Group
- Delete EC2 instances
- Delete Security Groups
- Delete key
- Delete IAM user
- Delete Linux user
