# Setup and Context

Our deployment diagram at the start of the lab:
![deployment diagram Lab 02](/units/03_db/c4-Deployment-Diagram-V2.0.png)

You have an IAM user with minimal permissions, a properly tuned security group, and a running website that you can tell from all the other sites.

Unfortunately, if anyone uses your site to login, they'll be sending their credentials in the clear, and if they use it to buy anything, they'll be sending their payment information in the clear. Let's fix that.

In this lab, we will create an SSL-offloading load-balancer so that your site can be accessed over HTTPS, so that users of it may send and receive sensitive information such as login credentials and payment information in an encrypted session.

### Areas of focus for this lab:

- The **LB Security Group** (lb-sg) set up
- Switch DNS A record to CNAME

![Security Group for LB](ccfun_LB-secgroup-2021-4-4.png)

---

1. In the [EC2 Dashboard](https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2), go to Load Balancers (menu on the left, scroll to 'Load Balancing' --> 'Load Balancers'... or type 'load balancers' in the top search field), and click **Create Load Balancer**

    Click "Create" under **Application Load Balancer (HTTP HTTPS)**

    Name it **$MACGUFFIN-lb**

2. In Network Mapping > Mappings, select at least two Availability Zones.

3. In Security groups, click "Create new security group" which opens in a new tab.

    Set Security group name to $MACGUFFIN-balancer-sg.

    Add **Inbound** rules for HTTP and HTTPS with Source "Anywhere-IPV4".

    Edit the default **Outbound** rule so that instead of anywhere, the lb security group can only make outbound connections to the $macguffin-web-sg group. This prevents our lb being able to make requests to anything other than our web hosts.

    Click **Create Security Group** and close the tab.

    Click the refreshing arrow to refresh the list of available security groups. Choose the group you just created ($MACGUFFIN-balancer-sg). Delete the default group.

4. In Listeners and routing, click "Create target group", which opens in a new tab.

    In Basic Configuration, choose Instances in Choose a target type.

    Set target group name to $MACGUFFIN-tg

    Scroll down to Health checks, expand `Advanced health check settings`, and change `Success codes` from "200" to "200,301"

    Click **Next** (big orange button)

    In Register Targets, click the checkbox for your server machine $MACGUFFIN-web and click `Include as pending below`.

    Click **Create target group** (big orange button) and close the tab.

    Click the refreshing arrow to refresh the list of available target groups.

    Click **Add Listener** and change the protocol on the new listener to `HTTPS` the port should automatically change to `443`

    For both listeners, choose the target group you just created ($MACGUFFIN-tg).

5. In Security listener settings, under Default SSL certificate, in the "Select a certificate" drop-down list-box, choose the certificate for ccf.sofree.us.

    The certificate should have been created for your use before class-time

6. Review the Summary. You should see artifacts of all the work you just did: Security Group, Target Group, and ACM certificate.

7. Finally, click **Create load balancer** then **View load balancer** (big orange buttons, both)

8. Select your balancer from the list

    Details of your lb will appear in a window below

    Locate the DNS name, copy it by clicking the copy icon

    ![image](ccfun_LB_DNS_Name-2021-4-6.png)

    Paste this into a new tab and hit enter (or Paste and Go, if you have that), now you've verified you can reach your website through your LB using HTTP.

9. Change your DNS A-record into a **CNAME** record, (make $MACGUFFIN.ccf.sofree.us an alias of the DNS name of your load balancer)

    Type 'route 53' in the top search bar, click the **Route 53** icon that pops up

    Click **Hosted zone** link, and click **ccf.sofree.us** link on the next screen

    Now you're in the ccf.sofree.us **DNS zone details** page

    Find your original A record (**$MACGUFFIN.sofree.us**), click the blue check box next to it

    An edit section appears in the lower part of the screen for the A record

    Ensure you change the key fields as depicted:

    ![Route53 Cname](Route53_CNAME_2021-4-5_2.png)

    1. Change the Record type to CNAME
    2. Change the Value to the FQDN of your Load Balancer (what you copied in the last step)

    Click **Save**

    You should now be able to browse your site at $MACGUFFIN.ccf.sofree.us over HTTP and HTTPS.

## Update Wordpress to prefer the HTTPS URL

1. WordPress is *very* domain-name-specific, before you can manage the site with HTTPS, you need to update its name in in WordPress General Settings

    Login at **$MACGUFFIN.ccf.sofree.us/wp-admin/** with the info you collected shortly after running boot-site

    Once you're logged in, click Settings in the left-nav

    Change **WordPress Address (URL)** and **Site Address (URL)** to "https://$MACGUFFIN.ccf.sofree.us". Now, navigate to the HTTPS URL, https://$MACGUFFIN.ccf.sofree.us.

## Update the Load Balancer to redirect all HTTP connections to HTTPS

    **CAUTION** Don't do this until you've updated the WordPress General Settings, and confirmed that you can browse and manage your site over HTTPS or you may get stuck in a redirect loop with the balancer redirecting you to HTTPS and the WordPress site redirecting you to HTTP.

    From the [AWS EC2 Console](https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2) On the left-nav go to **Load Balancing** --> **Load Balancers**

    Find your load balancer **$MACGUFFIN-lb** and click the button to the left of it, detail tabs of your LB will appear below

    Click the **Listeners** tab

1. Find the **HTTP 80** listener, click the button to the left of it, then click **Edit**

    ![image](ccfun_LB_Redirect_2021-4-10.png)

    On the next screen, delete the existing rule and then click **Add action** dropdown and select **Redirect to...**

    ![image](ccfun_LB_Redirect_2021-4-10_1.png) ![image](ccfun_LB_Redirect_2021-4-10_2.png)

    A new redirect rule will appear and **HTTPS** will automatically be selected, specify port **443** in the field next to it

    ![image](ccfun_LB_Redirect_2021-4-10_3.png)

    Browse to **http**://$MACGUFFIN.ccf.sofree.us and ensure that it redirects to **https**://$MACGUFFIN.ccf.sofree.us

## Tighten the webserver security group

1. Modify your webserver security group so that it only accepts HTTP from your balancer security group

    On left-side menu, under **Network & Security** --> **Security Groups**

    Find your webserver security group **MACGUFFIN-web-sg**, click the Security group ID next to it --> **Edit inbound rules**

    ![image](ccfun_LB_SecGroup_2021-4-10.png)

    Remove the HTTP from Anywhere rules and add HTTP from your balancer-sg

# Ending Deployment Diagram
![deployment diagram Lab 03](/units/04_lb/c4-Deployment-Diagram-v3.0.png)

|Back to units: [units](../../README.md#units)|
|:---:|
