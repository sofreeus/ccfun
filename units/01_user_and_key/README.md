# Setup and Context

**Choose or create your MacGuffin!** Before we do anything else, you need to pick a unique identifier for your imaginary software product or service. You'll use this to identify your cloud resources for the rest of this class. Use something that is short and easy to type, like 'falcon', 'gauntlet', 'godot', 'ring', 'rosebud', or 'time'. For the rest of this class we will refer to this as your "MacGuffin" and you may sometimes see it in notation as $MACGUFFIN - the $ is not something you type, rather bash notation for a variable. Throughout the text we will express placeholders as $BashVariables so whenever you see that you'll know it's a placeholder for something we've set previously.

**TO TEST FIRST** We've said our website isn't working, so to verify this let's navigate to $MACGUFFIN.ccf.sofree.us (your first chance to replace $MACGUFFIN with the unique identifier you chose above!) - the website should be down. Let's get to business getting our account set up and the website back up and running.

---

The CEO had set up an AWS account on the company card for you to experiment and get started with – he owns the billing account (you can create one yourself in real life if you ever want to) He had some help at least so he set up:

- An **IAM Admin** user, which we can use to create other IAM Users

  This is good because it creates security by separating permissions and making it so no one needs to log into the billing/primary admin/root account.

  You can't use this account for creating actual AWS resources, only for creating other IAM users.

- An **IAM Group** with all the needed permissions to create the AWS resources:


  + **AmazonEC2FullAccess** EC2 (elastic compute cloud) is where we will create the instance for our web app

  + **AmazonRDSFullAccess** RDS (relational database service) is where the database for our web app will eventually get moved to later in the class (for class we are setting this up now to save time later, in a real world situation we would not add this permission until we need it)

  + **AmazonRoute53FullAccess** So we can make our own DNS records using Amazon Route 53 which is a highly available and scalable cloud Domain Name System (DNS) web service. It provides a way to route end users to Internet applications by translating names like www.example.com into the numeric IP addresses like 192.0.1.1

  + **IAMReadOnlyAccess**  Your user needs to be able to read/verify its own permissions for certain functions

  + **AWSCertificateManagerReadOnly** Similar to IAMReadOnlyAccess, your user needs to be able to read auth certificates for certain functions

### User Types: quick summary

users(***permissions***):

- root (***unlimited***)
- iam user(***limited***)
- iam admin or billing admin(***special***)


![image](ccfun-root-iamuser-2021-4-3.png)

---

**[Lab exercise begins here]:**

unit steps:
- Create AWS IAM user
- Create ssh key pair and import the public key to EC2

## OPTIONAL secrets template

You will be managing more than a few different sets of credentials today. You will probably want to copy and paste them. You may want to use this file as a form for storing them.

[secrets file](ccf.secrets.TEMPLATE)

In real life, never keep secrets in a plain-text file. Keep them in BitWarden, KeePass, or another password manager. During the class, you can use a text file because you don't care if the secrets end up all over the Internet, because they're only useful today.

## Create AWS IAM user

*(if you want to test first try logging in with your identifier... you can't!)*

We'll log in as the IAM Admin (**ccfadmin**), create a new user, and put that user into the appropriate user group (**CCF-Admins** group), then log out, and log back in as the new **IAM user** we just created

1. Log in to the [AWS Console](https://sofreeus.signin.aws.amazon.com/console) as `ccfadmin` with the password provided by your instructor/s.

    In the search field at the top, type "iam"

1. Click **IAM - Manage access to AWS resources**

    Now you're in the IAM dashboard

1. In the left-side menu, under "Access management", click "Users"

    Click the "Add user" button

    Now you're in the "Add user" screen, enter the same username you use on Mattermost and manage.ccf.sofree.us

1. Under "Select AWS access type", select:

    **AWS Management Console access**

1. Under "Console password", choose Auto-generated password, or Custom password (be sure to save this password in a password manager or somewhere else)

1. Deselect "**Require password reset**", because you are creating your own account. Whenever you are creating an account for someone else, leave this option selected to avoid knowing their password long-term.

1. In the lower right corner, click **"Next: Permissions"**

1. On the next screen, **Add user to group** option should be pre-selected with some group options below it

    Check the box for **CCF-Admins** group

    > The 'attached policies' link gives more detail on the permissions granted by this group. Optionally, click and review the permissions you are inheriting by joining this group:

    ![policies link](ccfun-iamgroup-2021-4-3.png)

1. In the lower right corner, click **"Next: Tags"** ---> **"Next: Review"** ---> **"Create user"**

1. On the next page, click the button **"Download .csv"**

    This csv is your credentials in plain-text! Keep it secret! Keep it safe!

    **Log out and log back in as your new IAM user**

    Use the Console login link provided for you in the csv file, this url was generated just for your new IAM user

    If it works, then you are done with the **"Create user"** step!

## Create a Linux User and Secure Shell Key Pair

Open the shell program of your choice (**Bash, Zsh, Ksh, probably Putty or MobaXterm** Powershell for Windows, whatever... as long as it can SSH)

Secure Shell to `Manage.CCF.SoFree.Us` as `ccfadmin` and create a learner account for yourself using the `add-learner` script.

The conversation should look like this, except that instead of `alice`, you should put the username that you use on GitLab.com and Mattermost.

```text
$ ssh ccfadmin@manage.ccf.sofree.us
(ccfadmin@manage.ccf.sofree.us) Password:
[ccfadmin@Manage ~]$ sudo add-learner alice
Created user 'alice' with the password 'tmp.7rwIcFQaHP'
[ccfadmin@Manage ~]$ exit
logout
Connection to manage.ccf.sofree.us closed.
$ ssh alice@manage.ccf.sofree.us
(alice@manage.ccf.sofree.us) Password:
[alice@Manage ~]$
```

1. Once you are logged into the management station, create the key we'll use for authentication to the EC2 machine later.

    ```bash
    ssh-keygen
    ```

1. Accept the default key file location
1. Set a good password: easy to remember, hard to guess.

    You now have an SSH key pair in this folder: `~/.ssh`

1. List out the contents of this folder:

    ```bash
    ls ~/.ssh/
    ```

    Should look something like this:

    ```text
    id_rsa  id_rsa.pub
    ```

    **id_rsa** = private key

    **id_rsa.pub** = public key

    Next we'll import the public key into our AWS EC2 services

## Create EC2 Key

1. Navigate back to the [AWS main console](https://us-west-2.console.aws.amazon.com/)

    In the search field, type "ec2"

    Click the icon that pops up:
    **EC2 - Virtual servers in the cloud**

    You should be in the EC2 Dashboard

    IMPORTANT: Look to the upper right corner of the screen for the region menu and **ensure that you are in Oregon (us-west-2)** it should look like this:

    ![image](2021-03-06_11_26_00-Launch_instance_wizard_EC2_Management_Console.png)

1. In the left-side menu, scroll to "Network and security", click "Key pairs"

    In the upper right corner is a drop-down called "**Actions**", click and choose "**Import key pair**", name the key "**$MACGUFFIN-key**", where $MACGUFFIN is your actual MacGuffin: alicorn, briefcase, cash, egg, grail, money, power, time, treasure, zombies, or whatever.

    ![image](2021-03-06_11_35_51-EC2_Management_Console.png)

1. We need to copy the content of the **id_rsa.pub** key from the command line

    Navigate back to the **management station** and type the following at the prompt:

    ```bash
    cat ~/.ssh/id_rsa.pub
    ```

    The output should be similar to this:

    ```text
    ssh-rsa AAAAB...R+LWhuc4WE= username@manage
    ```

1. Copy all of the output from "**ssh-rsa...**" to the end.

1. Go back to the EC2 dashboard make sure you are still in the "**key pairs**" section, paste the contents of your public key into the Public key contents field (**the big rectangle**)

1. Click "**Import key pair**" in the lower right corner

  Verify that the public key that you imported appears in the list of key pairs in the AWS console

  If its there, you're done with the **Create key** step!

  ![image](2021-03-13-Key_pairs_EC2_Management_Console.png)

**[NOTE]:** here's a few steps that add a lot of convenience later on...

- Ensure you are logged into the management station and type in these commands (be ready to give the password for your private key when prompted):

  ```bash
  eval $( ssh-agent )
  ssh-add
  ```

  This adds your default key to an in-memory agent, so that you don't have to enter the decryption password every time you use it. But, it leaves the copy of your key that's stored on the disk, safely encrypted.

- To verify that your key has been added to your ssh-agent session, list added keys, like this:

  ```bash
  ssh-add -l
  # or
  ssh-add -L
  ```

To ensure we can just copy paste the commands from the subsequent units, its useful to export our MACGUFFIN environment variable now. If you've already added your MACGUFFIN to an rc file, like .bashrc or whatever, you can skip this step.

```bash
export MACGUFFIN=yourchosenmacguffin
echo $MACGUFFIN
```

In this example the console should print `yourchosenmacguffin`

After exporting our MACGUFFIN, to Automatically forward it through our SSH session into our web instance we add an entry into our config file. This tells ssh that any connection to the host below should automatically send the MACGUFFIN variable. Since we are running this on the shell, the $MACGUFFIN inside the echo will be replaced with the value of the MACGUFFIN variable.

```bash
echo "

Host $MACGUFFIN.ccf.sofree.us
    SendEnv MACGUFFIN
" >> ~/.ssh/config
```

In the next unit, we will modify an sshd_config parameter on the destination host to allow receipt of this variable.

|Back to units: [units](/README.md#units)|
|:---:|
