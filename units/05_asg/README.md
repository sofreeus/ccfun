# Setup and context

Here is our deployment diagram at the start of this step:
![deployment diagram Lab 03](/units/04_lb/c4-Deployment-Diagram-v3.0.png)

# Auto Scaling group

If the site got very busy, or the node got wedged or casually but persistently misconfigured, like if someone disabled the network service and rebooted, or whatever, we'd be pretty bummed and have to scramble. Let's get some redundancy and scaling.

---

An Auto Scaling group needs a Launch Template. A Launch Template needs an AMI (Amazon machine image)

## Prep for Auto Scaling Group: AMI and Target Groups

1. From the EC2 Dashboard, go to Instances, highlight your instance, then from Actions, Image and templates, choose **Create image**

    ![image](ccfun_Create_AMI_Image-2021-4-7_3.png?)

    A new pop up window will appear

    Name the image **$MACGUFFIN-ami** Capture the **Instance ID** that gets generated:

    ![image](ccfun_Create_AMI_Image-2-2021-4-7.png?)

1. Remove your instance from your **Target Group** and ensure that your website is down

    On the left-side menu of the AWS console, under LOAD BALANCING, click **Target Groups**

    On the **Target Groups** page, find your target group **$MACGUFFIN-tg**, click the button next to it

    Info about your target group should appear in a sub-window below

    Click the **Targets** tab in the sub-window and **deregister** as shown below:

    ![image](ccfun_Target_Group_Deregister-2-2021-4-7.png?)

    > From this point, we want our target group to contain only instances created by the Auto Scaling Group(ASG) that we will now create... not the manually created instance we've used up to now

## Create an Auto Scaling Group and Launch Template

1. From the left-side menu of EC2 Dashboard, go to Auto Scaling/Auto Scaling Groups and click **Create Auto Scaling Group** (big orange button in the upper right)

    Start creating an Auto Scaling Group named **$MACGUFFIN-asg**, then click "Create a launch template" which opens in a new tab.

    Fill out the template as shown:

    ![image](ccfun_Create_Launch_Template-2021-4-7_2.png?)

    In "Application and OS Images..."

    Click "My AMIs"

    > Search for the AMI you created in the last step... by AMI name ($MACGUFFIN-ami) or by AMI ID

    For Instance type, choose t3a.small, unless you have a good reason to choose something else. t3a.small is a perfect blend of price and performance for many smallish tasks.

    For Key pair, choose "$MACGUFFIN-key" (or whatever you named your key pair)

    In "Advanced network configuration", "Network interface 1"

    * Set "Auto-assign Public IP" to "Disable"
    * Set "Delete on termination" to "Yes"
    * Set Security group to "$MACGUFFIN-web-sg" (or whatever you named your web-server security group)

    ![Network interfaces showing Web SG and public IP Disabled](ccfun_Create_Launch_Template-2021-4-7_4.png?)

    Finally, click **Create Launch Template** and close the tab

    > Resume creating the ASG...

1. Click the refreshing arrow next to the Launch template selector

    Find and select your launch template

    > Note that this interface is unaware of your carefully selected Security Group, and it may cause problems later

    Click **Next**

    In "Choose instance launch options":

    In "Availability Zones and subnets", select the subnets the Load Balancer is attached to, probably us-west-2{a..d}

    ![image](ccfun_ASG_Launch_Template_2021-4-7_2.png?)

    Click **Next**

    In "Configure advanced options":

    Choose Attach to an existing load balancer and choose "$MACGUFFIN-tg" (or whatever you named your target group) in the drop-down

    ![image](ccfun_ASG_Launch_Template_2021-4-7_3.png?)

    Click **Next**

    In "Configure group size and scaling policies":

    Set Desired and Minimum to 2 and Maximum to 4

    In Scaling policies, choose **Target tracking scaling policy** and leave the Metric type set to Average CPU utilization and Target value to 50

    > Later, if time allows, we'll run a CPU burner on a node to drive CPU utilization high and observe automatic scale up.

    Nothing to do in "Add notifications" or "Add tags".

    Review carefully in "Review" and click **Create**

    > You should see your new auto scaling group with a Status of "Updating capacity"

## Watch the Auto Scaling Group in action

If you look in Instances, you'll see instances with no names

1. Click the gear icon below the "Launch instances" button, in the **Tag columns** drop-down choose **aws:autoscaling:groupName**

    > Now, you can see that some of the nameless instances are members of your new auto scaling group

    > After a minute or so, the ASG status goes to '-' and the members in Instances will show "2/2 checks passed"

1. If you see all targets in the target group as unhealthy in the Target Group, but they show good in Instances, change the "Success codes" to **200,301** in Group details --> Health check settings

    Click 'Edit', then open "Advanced health check settings" and change Success codes from "200" to "200,301"

    > With your manually created machine no longer in the TG, all targets are clones created from the image by the ASG monitor

1. Test that your website is up

    Browse or curl $MACGUFFIN.sofree.us

## Auto Scaling back up to the floor.

1. Terminate an ASG instance

    Test that your website is up

    Terminate the other ASG instance

    Test that your website is down

    > Wait a few minutes and see that the ASG monitor has replenished its membership, using the launch group, and that your website is back up

## Auto Scaling up past the floor (if time allows)

1. Create a management security group: **$MACGUFFIN-mgmt-sg**

    Add a rule to your management security group to allow Secure Shell requests from Manage.CCF.SoFree.Us.

    Add a rule to your webserver security group ($MACGUFFIN-web-sg) to allow Secure Shell requests the management security group ($MACGUFFIN-mgmt-sg).

    Remove the old rule from your webserver security group that allows Secure Shell requests from Manage.CCF.SoFree.Us.

    Remove your reference machine from the web-sg.

    Add your reference machine to the mgmt-sg.

    You're going to use your reference machine as a management station, an ssh proxy, or jump box to reach the ASG members.

    > Remember they have no public IP addresses!

1. From manage.ccf.sofree.us, make sure you have an ssh-agent running.

    ```bash
    ssh-add -l          # a good way to check status
    eval $( ssh-agent ) # starts an ssh-agent
    echo $SSH_AGENT_PID # shows whether you have an SSH Agent started
    ssh-add             # adds your default key to the agent/keyring
    ssh-add -l          # shows your loaded keys
    ```

    With an ssh-agent running and your key loaded into it, ssh to the reference machine, providing the -A option to forward your agent. `ssh -A debian@reference-machine-external-ip`

2. From your reference machine, download stress-ng and it's dependencies.

    ```bash
    apt download libipsec-mb0 libsctp1 stress-ng
    ```

3. Once for each member of your Auto Scaling Group

    ```bash
    # Send the stress-ng files to the ASG member machine
    scp *.deb $ASG_MEMBER_IP:
    # Secure shell to the ASG member machine
    ssh $ASG_MEMBER_IP
    # Install stress-ng
    sudo dpkg -i *.deb
    # Set up a CPU burner to run for 10 minutes
    stress-ng --matrix 0 -t 10m &
    # Quit the session, leaving the CPU burner running
    exit # or just Control+D
    ```

    > After several minutes you'll see your ASG scale up, probably to 4 nodes

    > After several more minutes, the CPU burners will quit, and the ASG monitor will notice, and eventually, you'll see your ASG scale back down to the floor.

# Ending Deployment Diagram

Here is our deployment diagram at the end of this lab:
![deployment diagram Lab 04](/units/05_asg/c4-Deployment-Diagram-v4.0.png)

|Back to units: [units](/README.md#units)|
|:---:|
