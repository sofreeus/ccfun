# Setup and Context - A Gentle Introduction

**Freedom Software Inc** is a small custom and bespoke software company.

Despite a lot of problems the company has been growing and changing and the founders were finally starting to retire. They recently got a new first time CEO who was really driven to bring the company into the modern era and start scaling up to better suit their customers and permit growth.
Despite having a product customers love, the company was developing software and running it’s business like the stone ages.

The one person who made up essentially the entire IT department since it was founded – maybe he’s a sysadmin but maybe actually the CTO, hard to tell – was really old school and decided it was time to retire - a combination of the IPO and not getting along with the new CEO.

*You* are hired on as the new Sysadmin by the CEO – she is looking for someone like herself with not a lot of experience but great potential so the two of you can grow in your roles together – you into officially becoming CTO – need to prove yourself. CEO has read about the benefits of “the cloud” and wants to see what it will take to move us there.

# What is the cloud?

- The cloud is made up of datacenters around the world and gives us the ability to host virtual machines and scale globally
- Cloud providers divide up these datacenters into regions and inside those regions are multiple availability zones, so you can keep your website up even if there’s a problem in one region and/or zone.
- This means you can recovery from disaster more easily
- And means high availability – both in different conditions, but also for customers located around the world
- You can snapshot and backup and restore and recovery more easily.
- And pay for what you need – you can scale things up when you need a lot of them – and scale down when you don’t.
- We call a virtual machine in the cloud an instance - it exists when you need it, and is not permanent both from the perspective of recoverability (servers are livestock not pets) and the ephemeral nature of computing resources in the cloud.
- You can scale instances horizontally or vertically.
  * Vertically is like getting BETTER, more powerful computers.
  * Horizontally is like getting MORE computers.
  * You can do either or both – but it’s best to optimize


# Getting to Work

Hired during Covid so only briefly able to go into the office to get a few passwords and retrieve a few things – you are greeted with various “server rooms” that look like this:

![old sysadmins space](Winner1.png)

You were given some files and information, a few password lists, how to get off-site backups, standard steps to remotely restart systems and schedules, and supposedly there is more information somewhere in his office – maybe you’ll look again if there’s an emergency or once the offices open back up.

Thankfully the systems do seem fairly resilient for what they are, and you haven't had to deal with any outages that the scripts couldn't help with.

The company is making a big announcement in a week and after that you intend to start making changes and experimenting with things when there’s less risk.

## MONDAY MORNING

### 0445 AM MDT

#### 1 WEEK BEFORE BIG ANNOUNCEMENT

Monday morning at 0445 MDT a week before the big announcement and you try to log on from home and you can’t connect. You follow the protocols, run the scripts, wait and… nothing.

Website is down.

Email is down.

Code repository is down.

Everything

Is

Down

You’re an early riser, maybe you can figure out what happened and get it back up and running before anyone notices.

Your security footage goes to the cloud so you log on and start reviewing footage for the last 24 hours.

Nothing of interest, no movement, when you see it - only 30 minutes before your first log on attempt at 0414 MDT -

[![COMPUTER DESTRUCTION](http://img.youtube.com/vi/ehcGWLOH-Js/0.jpg)](http://www.youtube.com/watch?v=ehcGWLOH-Js&t=445s "Video Title")
*click to see the video*

You later learn the sprinklers did eventually kick on, but a big chunk of the office was destroyed including 3 ‘server’ rooms and all their equipment.

## Making Sense

You once got some emergency preparedness training and they said in emergency situations we should always STOP - stop, think, observe, and plan.

**Stop** - Big breath, we'll get through this!

**Think** - what might be useful?

You once worked with an Agile Coach who told you about the Cynefin Sense Making Framework:

![cynefin1](2021-cynefin-framework-scaled-1024x1024-1-600x600.png)

![cynefin2](2021 cynefin alt picture.jpg)

**Observe** -

You are in **chaos** so you need to **ACT** fast to recover the website, then you can **SENSE** what we should do longer term and **RESPOND** with some design and more comprehensive set up.

You know there's no possibility of entering the building, you can't recover anything anyway, but you know you need the website up and running ASAP.

Maybe this is an opportunity!

You can migrate to the cloud!

"We pretty much have to anyway" you think to yourself - and maybe you can get it up and running before anyone notices.

That would really make you look good to the CEO.

**Plan** -

The plan - stand up the website in the cloud as fast as possible, we'll figure out the rest afterwards.

Before we can even stand up the website though there are some [BASICS](/units/01_user_and_key/README.md) that we need.

|Back to units: [units](/README.md#units)|
|:---:|

:)
