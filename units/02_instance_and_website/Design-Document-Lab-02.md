# Design Document

## Title: Reliable Scalable Wordpress Site

**Date:** 3/20/2021

**Authors**: Ed Schaefer

**Revision Number:** 1.0

**Status:** In Progress

### Executive Summary:
Create a reliable and scalable Wordpress in AWS Cloud, including redundancy and security.

### Goals:
* Database reliable and restorable quickly and safely
* secure connection for customers and employees
* autoscaling to respond to large amounts of traffic or DDOS attacks

### Non-goals:
* Restoring other software systems
* Integration with other systems

### Background:
* AWS - Amazon web services - Amazon's Cloud
* EC2 instance - virtual server in the Cloud
* Route 53 - a scalable and highly available Domain Name System (DNS) service
* Wordpress -
* Docker Compose -
* RDS database - Amazon Relational Database Service (or Amazon RDS) is a distributed relational database service designed to simplify the setup, operation, and scaling of a relational database for use in applications.
* Autoscaling - dynamically adjusts the amount of computational resources in a server farm - typically measured by the number of active servers - automatically based on the load on the farm

### High-Level Design: (a brief, high-level description of the design; diagrams are helpful)
<details>
  <summary>Click here for System Context Diagram</summary>
![system context diagram](c4-System-Context-Diagram.png)
</details>

<details>
  <summary>Click here for Container Diagram</summary>
![container diagram](c4-Container-Diagram.png)
</details>

### Detailed Design:
<details>
  <summary>Click here for complete deployment diagram</summary>
![deployment diagram](/units/05_asg/c4-Deployment-Diagram-v4.0.png)
</details>
