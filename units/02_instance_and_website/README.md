## The resources

Let's talk about security groups...

![image](ccfun_secgroup_simple-2021-3-7_2.png)

At the end of this unit, each student's webserver security group should have:

- recognizable name in the $MACGUFFIN of the student. i.e. alicorn-web-sg or rug-web-sg

- HTTP, Source "Anywhere"
- SSH, Source "My IP" / management station IP

Lab steps:
- Create instance and security group
- Configure instance into a webserver

## Create Instance

1. From AWS console home, click '**EC2**' or type '**ec2**' in the search bar at the top and click '**EC2 - Virtual Servers in the Cloud**' icon that pops up

1. From the EC2 dashboard, click '**Launch instance**' (big orange button)

    A workflow should appear with 7 steps

1. The first workflow step is '**Choose AMI**'

    Paste this Amazon Machine Image ID into the search bar: `ami-0c7ea5497c02abcaf`. This is the debian image we will use for the webserver.

    > There should be one result in 'Community AMIs'. If you do not see this result, verify your region is set to "Oregon" (us-west-2) in the menu in upper right corner of the page

    **single-cloud vs multi-cloud decision point:** Community AMIs vs AWS Marketplace

1. Click on '**Community AMIs**' on the left side

    The associated instance should appear in the center, called 'Debian 10 (HVM), SSD Volume Type'

    Click the **big blue 'Select' button** to the right of it

    This takes us to step 2, 'Choose Instance Type'

1. **Instance Type** step is all about how much resources to allocate to this

    The '**t2.micro**' option should be pre-selected

    In the lower right corner, select '**Next: Configure Instance Details**' which takes you to step 3, 'Configure Instance'

1. For **Configure Instance** step, leave all the options at default, scroll to the bottom right

    Click 'Next: Add Storage' to go to step 4

1. For **Add Storage** step, leave this as is

    Click 'Next: Add Tags' to go to step 5

1. For **Add Tags** step, click '**click to add a Name Tag**' and name your instance **$MACGUFFIN-web** (short for webserver)

    ![image](ccfun_Instance_Name_Tag-2021-4-5.png)

    Click 'Next: Configure Security Group' in the lower right, this will take you to step 6, 'Configure Security Group'

1. For **Configure Security Group** step, name the security group **$MACGUFFIN-web-sg**

    'Create New Security Group' option is pre-selected for us and so is SSH (port 22)

    > We don't want SSH access to be open to the world so we want our security group to restrict the number of sources that can access AWS through the security group down to exactly one... namely our management station! So we need to get the external IP address of our management station and put it in the source portion of SSH for the Security Group

    Go back to your **management station** prompt, type in this command:

    ```bash
    curl ifconfig.me && echo
    ```
    > The result should look like this:

    ```
    [zee@ccfun-mgmt ~]$ curl ifconfig.me && echo
    34.233.248.99
    ```

    Copy the IP address from the bottom of the result (from your management station not from above, right?)

    > This is the public-facing IP address of your management station instance

    Go back to your AWS console (still in the middle of setting up a Security Group)

    Under the '**Source**' drop-down for SSH, paste in the **public IP address** you just copied and add '**/32**'

    > If you have more than one management station, add one SSH allow rule per management station

    Click 'Add Rule' button in the lower left to go to step 7

1.  For the **Add Rule** step, in the '**Type**' drop-down, select '**HTTP**' and ensure that the '**Source**' drop-down for this also specifies '**Anywhere**'

    ![rules screenshot](2021-03-16 18_35_09-Launch instance wizard _ EC2 Management Console.png)

    Click '**Review and Launch**' in the lower right

    > On Step 7 Everything should look like this, if so you are ready to click 'Launch' in the lower right

    ![Review Instance Launch](review-instance-launch.png)

    > A pop up for selecting a key pair will appear

    Ensure that '**Choose an existing key-pair**' option is selected and that the key pair you created earlier is specified

    Check the acknowledgment and then click 'Launch Instances'

    You'll be taken to a 'Launch Status' homepage

1. At the **Launch Status** page, in the lower right, click 'View Instances'

    You'll be taken to the 'Instances' page where you should see your instance being created

    Locate the **Public IPv4** address of your newly created instance in the Details tab, under Public IPv4 address

    **Copy the IP address** by clicking the copy icon to its left

## Create a DNS record

Add an A record for your server to DNS.

For **Route53**, you'll put your new web address and corresponding IP address (the ones you saved from the previous step) into AWS' DNS system

1. In AWS console search field, type in **route53** and click the icon that pops up

    You should be in the Route53 dashboard, click on **Hosted zone** --> **ccf.sofree.us**

2. Click **Create record** (big orange button on the right)

3. Put your MacGuffin in the **Record name** field, so that the whole record name is $MACGUFFIN.ccf.sofree.us

    Example: If your MacGuffin is 'rug', put 'rug' so that the whole record name is rug.ccf.sofree.us

4. Put your server IP address in the **Value** field

    Change the **TTL(seconds)** value to to 30 (we are making this intentionally short for class so things will update faster than normal) :

    ![pic](Route53_CreateRecord-2021-3-30_2.png)

5. Click **Create records** (big orange button)

6. At the management station, server machine, or any machine with dig, run the following

    ```bash
    dig $MACGUFFIN.ccf.sofree.us +short
    ```

    Confirm that your server IP returns. If it doesn't, fix your DNS entry.

## Test the Security Group

If you didn't do it at the end of the last lab, and you don't want to be prompted for the password every time you use your password-protected private key, do this now:

```bash
eval $( ssh-agent )
ssh-add
# verify that the key is loaded
ssh-add -l
```

From Manage.CCF.SoFree.Us, SSH into your EC2 machine

```bash
ssh admin@$MACGUFFIN.ccf.sofree.us
```

> Normally we'd add user accounts for all the sysadmins in the company to the instance here, and remove the built-in admin, but since this machine only has one sysadmin (you), repudiation is not a likely problem, so we can just go ahead and abuse the admin account. When you have more than one sysadmin, and definitely when you have more than two, add a unique, un-shared user account for each sysadmin.

When the prompt changes to something like this: `admin@ip-172-31-3-165:~$` it means you are logged into your new debian EC2 instance so **your SSH allow rule must have worked**!

### Enable auto-forwarding of the MACGUFFIN variable from the management station

We need to modify a configuration parameter here to allow the ssh session from the management station to automatically forward the MACGUFFIN variable to this host. The snippet below adds this parameter and restarts the sshd service so its applied.

```bash
echo 'AcceptEnv MACGUFFIN' >> /etc/ssh/sshd_config && systemctl restart sshd
```

Lets log out and log back into this host again using the same snippet above

```bash
ssh admin@$MACGUFFIN.ccf.sofree.us
```

Back on the deployed instance again, lets check if the variable was forwarded through the ssh connection.

```bash
env | grep MACGUFFIN
```

You should see `MACGUFFIN=yourchosenmacguffin`

### Test HTTP access

Now, let's test HTTP access

Run these commands on $MACGUFFIN.ccf.sofree.us to ensure HTTP is allowed through the security group from anywhere

```bash
echo hello world from $MACGUFFIN > index.html
sudo python3 -m http.server 80
```

From any other machine in the world, browse or curl $MACGUFFIN.ccf.sofree.us

> If that works, you know that your HTTP allow rule worked, and you're ready to setup your website

Press **control+c** to stop the http.server test, and press **control+d** to end your secure shell session

## Create website

You recently took a class based around free and open source software and one of the resources made available to you was a Docker Compose template to set up Wordpress - lucky coincidence that the company website also runs Wordpress! You're not an expert on containers (yet!) and don't have time to learn (right now!) but the documentation says that running the boot-site script will create two containers - one running the wordpress installation and the other with a MySQL database.

1. Copy the configuration files from the class materials to your server machine.

    ```
    # From the **management station**
    # clone the class materials into your home folder
    cd
    git clone https://gitlab.com/sofreeus/ccfun.git
    # and use `scp` to secure copy the files up to your server machine
    cd ~/ccfun/units/files/
    scp * admin@$MACGUFFIN.ccf.sofree.us:
    ```

1. Boot up your site

    ```bash
    # secure shell to the server
    ssh admin@$MACGUFFIN.ccf.sofree.us
    # Verify the files you just copied over are there
    ls
    #Activate boot script
    ./boot-site
    ```

    After it completes, run a command to view your running Docker containers:

    ```bash
    cd ~/site/
    docker-compose ps
    ```
    Ensure that you see two items in the list: a web app container and a database container

1. Use a graphical web-browser to browse your server's FQDN

    If the **WordPress Admin/install page** comes up, you win!

1. **Configure your site** so that you'll be able to tell it from all the other sites

    After you click through the country page

    Give it a site title, with **your macguffin** and maybe your name, anything so it stands out to you

    Give it a user name and e-mail, save the user name and the auto-generated password somewhere safe!

    Check the box to **discourage search engine visibility**

    Click **Install WordPress**

    Success! Click **log in**, give your credentials

    > Now you should be in your website dashboard

1. Click **Settings** and then **General**

    When complete, it should look like this:

    ![macguffin.ccf.sofree.us](Macguffin-ccf-sofree-us-2021-4-4.png)

    If you make changes, scroll to the bottom and click **Save Changes**

1. Navigate to a browser and paste your web address in

    If your website's **Hello world** page appears, you did it! We won't need our webserver IP address from this point forward

## Deployment Diagram
Here's what we've implemented so far:

![deployment diagram Lab 01](c4-Deployment-Diagram-v1.0.png)

# Discussion and Next steps

## Congratulations

Finally our website is back up and running!

We can breath a brief sigh of relief, the CEO texted and said great work he didn’t expect it to be up so quickly, we’ve done a good job.

## Incident Report / Post Mortem

We want to get some good practices in place so we’ve established this incident report, which we have a sample/example of here.

[Outage Post Mortem](Outage-Post-Mortem.md)

## Threat Modeling

We want to enhance the reliability and resiliency of our site going forward, so let’s take a moment to do some design and figure out what that should look like.

After a round of threat modeling we identified the biggest risks to our website include:
* Database redundancy, backups, and the ability to scale as we grow
* secure connection for customer web traffic and employee logins
* website able to scale automatically to handle variable traffic, especially for announcements and new launches

## Design

After some research and many discussions with some colleagues at other organizations (along with lots of Twitter interactions and whiteboard brainstorming) you think you have identified your design. It should be relatively simple to set up, but far more reliable than the old setup.

We want to start using design documentation, here is a sample:

[Design Document](Design-Document-Lab-02.md)

This gives us a high level picture of what we will be doing for the rest of the units. In each lab we will look at a deployment diagram for that single step to help us keep track of what we are doing.

|Back to units: [units](/README.md#units)|
|:---:|
