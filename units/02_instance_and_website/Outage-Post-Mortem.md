# Post Mortem Fire

## Laptop Fire
### Incident # 0001

## Authors / Written By
Ed Schaefer

## Reporting Status
Wordpress site is up and running and available to customers.

## Date
3/19/2021

## Executive summary
An improperly stored laptop exploded resulting in a building fire and website outage. The website has been restored utilizing AWS and the cloud.
### Brief Description
A laptop was left plugged in and overcharging appears to have caused it to explode. The sprinklers did not start until the fire had spread into multiple rooms resulting in server destruction leading to the outage.
### Outage Description
Website was unavailable for 90 minutes to all customers and internal users. Upon restoration to the cloud all services were restored.
### What happened
Outage was discovered when Sysadmin attempted to log-in during the early morning. Reviewing security footage showed the laptop explosion and resulting fire until cameras were rendered inoperable. Sysadmin set up AWS admin account and EC2 instance with Wordpress installation using a widely available Docker Composition and restoring the website from backup.

## Background
### Contributing Conditions
- Laptops improperly stored
- cluttered server rooms
- no redundancy or automatic recovery
- all servers on premises, single point of failure

## Timeline
### Start Time
3/20/2021 0415 MDT
### End Time
3/20/2021 0545 MDT
### Chain of Events
- 0415 fire started
- 0445 attempted to log in
- 0455 started reviewing security footage
- 0510 discovered fire and Outage
- 0515 admin account ready in AWS
- 0525 EC2 Instance running
- 0535 Wordpress running
- 0545 Wordpress backup restored
### Duration
90 minutes
### Error Budget
Original - 540 minutes per year (0.999 availability)
Used - 90 minutes
Remaining - 450 minutes

## Impact
### End User Impact and Users Affected
Customers were unable to access the website or their accounts for 90 minutes.
All customers impacted.
### Infrastructure Impact
All existing infrastructure destroyed.
Infrastructure migrated to AWS EC2 instance.
### Productivity Impact
Minimal if any impact to employees due to timing of outage.
Likely will ultimately lead to productivity gain for Sysadmin and technology.

## Review
### What went well?
- Timing of incident and discovery
- Quick response
- Restored before many customers or employees could have noticed
### What could have gone better?
- total loss of hardware, documentation, and other information
- more recent backup
### Where did we get lucky?
- Timing of the occurrence - if this happened for upcoming announcement could be catastrophic
### What went wrong?
- nothing of note
### Process Breakdowns
- redundancy
- notifications
- storage of hardware
- disorganized workspace
### Lessons Learned
- already knew we needed to move to the cloud, this is forcing it
- make systems redundant, and not rely on single points of failure
- take backups more regularly
- valuable to plan and test disaster recovery more thoroughly

## Root Causes
- lack of organization
- poor procedures regarding long term hardware storage
- poor planning
- lack of future proofing
- unreliable hardware and architecture
- unreliable notifications
### Trigger / What caused the incident?
- improperly stored laptop plugged in resulting in battery overload and explosion

## Time to Detect - TTD
30 minutes
## Detection
Attempting to log in - coincidence

## Time to Mitigate - TTM
50 minutes
## Mitigation
Stood up EC2 instance with Wordpress running

## Time to resolve - TTR
90 minutes
## Resolution
Restored Wordpress backup to EC2 instance

## Recommendations
- Threat analysis
- Improve Wordpress implementation
- Apply learnings to other systems impacted

## Action Items
### and follow ups
- item
- type (mitigate, prevent, process, other)
- who
- priority
- due date

## Open Questions

## Who was involved

## Supporting Documents
