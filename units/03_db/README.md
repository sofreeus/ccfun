#

## Setup and context

Deployment Diagram at start of lab:

![Deployment Diagram Lab 01](/units/02_instance_and_website/c4-Deployment-Diagram-v1.0.png)

## Talk

Separating our database from the web app is an important next step.

Why?

1) Its easier to manage a database that way. Especially since we'll be using AWS' RDS service (a.k.a. DBaaS) to do it. RDS will manage the really tough aspects of long term data availability and resiliency, like: setting up the database cluster in multiple availability zones, taking nightly snapshots, setting up basic monitoring and an easy fail-over mechanism.

2) More than just making it easier, it's critical to understand that **we won't be able to scale our web app horizontally if we don't set up a centralized DB**

Ultimately, we want to be able to quickly add more instances of our web app as demand increases. But if each web app is writing to its own on-board database (our current architecture), then each DB will contain different data, and we won't be able to reconcile any of it into a single coherent view.

Also, we could never delete any copies of our web app. If we did, we would also lose unique data that only resides in that one copy of the app. With public cloud, we don't EVER want to be tied to any particular instances of a web app. We want them to be able to scale up and down to match demand and only the one database remains permanent. So we need a centralized DB, separate from but connected to ALL our web app instances.

### Area of focus for this lab

The **RDS Security Group** (RDS-SG)

Get the right variables into the right spots

![Security Group for RDS](ccfun_RDS-secgroup-2021-4-4_2.png)

## Create an RDS cluster

1. In the AWS console, click on **Services --> Database --> RDS** (or go to the free-form search field and type "rds" and then click the **RDS** icon)

    Click **Create database** (big orange button)

    > Because there are so many of us creating databases at the same time we may get an error for rate limiting RDS creation - just be patient and try again after a few minutes. During this time our recommendation is to either have someone in your mob share their screen and you all work through it together, or read through the rest of the lab.

2. **Standard create** should be pre-selected, leave that as is

    Ensure **Amazon Aurora** and **Amazon Aurora with MySQL compatibility** are also selected

    Scroll through the rest of the set-up template, make changes at these key spots:

    - **Templates** Dev/Test

    - **Settings --> DB cluster identifier**, give your DB a name `$MACGUFFIN-data`

    - **Auto-generate Password** make sure this is checked.

    - **DB instance class** choose "Burstable classes" > db.t3.small

    - **Availability & durability** choose **Create an Aurora Replica or Reader node in a different AZ**, this will ensure multi-AZ deployment *(this is what public cloud does best!)*

    - **Connectivity**
        * Public access: No <- This is the default, don't change it without a good reason
        * VPC security group: Create new: `$MACGUFFIN-data-sg`

    - Click **Additional Information** to expand it
        * Under **Monitoring** Un-check **Enable Enhanced monitoring**
        * Under **Deletion protection** Un-check **Enable deletion protection** (makes class cleanup faster)

    - Click **Create database** (big orange button)

3. Record your RDS cluster credentials.

    Click **View connection details**. White button on blue or green field labeled Creating database... or Successfully created datbase...

    You will need the following:

    |||
    |---|---|
    | RDS_HOSTNAME | Writer endpoint |
    | RDS_USERNAME | cluster admin username, usually 'admin' |
    | RDS_PASSWORD | cluster admin password, random or chosen |

    If it's too late to catch the admin password, **Modify** the writer Node of the cluster and reset it.

    If it's too early to get the Writer endpoint, you can also get it from cluster properties (Connectivity & security > Endpoints) later.

4. Correct the allowed source in the $MACGUFFIN-data-sg Security Group.

    1. Navigate to **EC2 --> Security Groups** and locate the database security group you just created for your RDS instance: **$MACGUFFIN-data-sg**
    2. Click the **Security group ID** to the left of the name.
    3. Click **Edit inbound rules** button in the lower right.
    4. Click the 'X' on the entry for your IP address to remove it.
    5. In the source box, type part of your MacGuffin, then choose your **webserver security group** which should be named something like: **$MACGUFFIN-web-sg** ![image](2021-03-13-RDS_SecGroup_add_webSG.png)
    6. Click the orange **Save rules** button in the lower right

## Test connectivity from db container of webserver to the RDS cluster

From the management station:

```bash
ssh admin@$MACGUFFIN.ccf.sofree.us
```

Heads up: For several upcoming steps, the instructions say $RDS_HOSTNAME $RDS_USERNAME $RDS_PASSWORD. You can put the values in manually, or set environment variables, at your option. If you choose to set environment variables, so that you can copy and paste, edit this statement to match your actual values and run it on your webserver.

```bash
export RDS_HOSTNAME=. RDS_USERNAME=admin RDS_PASSWORD=.
```

and let's check to make sure it is in there before we move on

```bash
echo hostname=$RDS_HOSTNAME user=$RDS_USERNAME pass=$RDS_PASSWORD
```

if you see what you're expecting we can move on

You'll notice we are using docker's `exec` to run a command directly inside our existing dbms container. This helps us avoid installing the mysql cli on our management or host machines to communicate with our remote RDS instance.

```bash
cd ~/site/
docker-compose ps
# test mysql connectivity from the dbms container to the RDS cluster
docker-compose exec dbms mysql \
--host=$RDS_HOSTNAME \
--user=$RDS_USERNAME \
--password=$RDS_PASSWORD
# example:
# docker-compose exec dbms mysql --host=planko-data.cluster-cadhsmpq4g83.us-west-2.rds.amazonaws.com --user=admin --password=Fu9mogi4De2Lu6Th
```

If you see something like this, it worked!:

```text
Welcome to the MySQL monitor...

mysql>
```

```bash
show databases
```

Type Control+D or "exit".

## Copy the wpdb database from the dbms container to the RDS cluster

### Backup WPDB from the dbms container

```bash
cd ~/site/
# the mysql root password was recorded for safekeeping by the setup script
ROOTPW=$( cat rootpw | cut -f 2 -d ' ' )
docker-compose ps
docker-compose stop wordpress # ensure that the app isn't running while the database is being moved
docker-compose exec dbms mysqldump \
  --databases wpdb \
  --password=$ROOTPW -r wpdb.sql
docker cp site_dbms_1:wpdb.sql .
```

### Restore WPDB to the RDS cluster

Recall the earlier command you used to test connectivity, and add "-T" after exec and "< wpdb.sql" to the end.

```bash
docker-compose exec -T dbms mysql \
--host=$RDS_HOSTNAME \
--user=$RDS_USERNAME \
--password=$RDS_PASSWORD < wpdb.sql
```

If the restore went well, you now have a 'wpdb' database. Check:

1. Run the connectivity-test command
2. In the mysql session, run "show databases;"
3. Verify that you see `wpdb`
4. Quit with ^d or 'exit'

### Create the WordPress Database User

```bash
# Create a short SQL script to create the user and grant privileges

export WORDPRESS_DB_PASSWORD=$(
grep WORDPRESS_DB_PASSWORD docker-compose.yml |
awk '{ print $2 }'
)

echo "
CREATE USER 'wpuser'@'%' IDENTIFIED BY '$WORDPRESS_DB_PASSWORD';
GRANT USAGE ON *.* TO 'wpuser'@'%';
GRANT ALL PRIVILEGES ON wpdb.* TO 'wpuser'@'%';
FLUSH PRIVILEGES;
" > user.sql

cat user.sql # and review *carefully*
```

Use approximately the same expression you used to create the wpdb database, to create the WordPress DB user.

```bash
docker-compose exec -T dbms mysql \
--host=$RDS_HOSTNAME \
--user=$RDS_USERNAME \
--password=$RDS_PASSWORD < user.sql
```

You should now be able to login to the wpdb database as the WordPress DB user. Try this.

```bash
docker-compose exec dbms mysql \
--host=$RDS_HOSTNAME \
--user=wpuser \
--password=$WORDPRESS_DB_PASSWORD wpdb
```

If you get a mysql prompt, you've successfully created the wpdb database and wpuser user.

Run "show tables;" just for the heck of it, and then ^D or 'exit' to exit.

## Reconfigure the app to use the RDS cluster

Take Down

```bash
# now lets get back into our webserver
ssh admin@$MACGUFFIN.ccf.sofree.us
cd ~/site/
docker-compose down
```

Reconfigure

```bash
cp docker-compose.yml docker-compose.yml.localdbms
vi docker-compose.yml

# change WORDPRESS_DB_HOST from 'dbms' to $RDS_HOSTNAME

# remove everything from 'dbms' to the end of the file
# ... using dG
```

NOTE: the above uses **vi** text editor to edit a file

If you've never used vi before, here's the basics:

- After you open the file in vi, you are in command mode:
  * 'dG' deletes from this line to the end of the file
  * 'dw' deletes this word
  * 'd2w' deletes two words
  * Press i to insert text
  * Start typing...
  * Press Esc to stop inserting and return to command mode
  * 'ZZ' : Shift+zz is save and exit

Bring up

```bash
docker-compose up -d
```

# Tests

```bash
curl http://MACGUFFIN.ccf.sofree.us/ # or browse
```

# Cleanup (optional)

After this lab succeeds: *.sql, rootpw, and db/ may be deleted from site/

And expressions with passwords should be deleted from bash history using `history -d`, and from ~/.bash_history with a text editor.

# Ending Deployment Diagram

Here is a picture of our deployment at the end of this lab:

![deployment diagram Lab 02](/units/03_db/c4-Deployment-Diagram-V2.0.png)

|Back to units: [units](/README.md#units)|
|:---:|
