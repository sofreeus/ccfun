# Cloud Computing Fundamentals
## Teaching Objectives / Learning Objectives

* what is the cloud
* what is AWS / cloud options / why we're using AWS
* user groups, security, permissions for admin / creating instances
* what is an instance
* creating an instance
* installing an application on the instance
* what is database vs RDS what does that mean
* splitting database from 'monolith' and putting on RDS
* what is a proxy / load balancer - why
* setting up a proxy / load balancer - SSL Certificate + distribute traffic
* what is autoscaling groups/how does that works
* set up autoscaling groups
* very basics of cynefin sense making model
* very basics of doing initial architecture and design
* very basics of incident reporting / responding to incidents
* very basics of diagramming
* very basics of design documentation
* very basics of planning
* very basics of site reliability engineering
