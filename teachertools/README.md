# Teacher Tools for Cloud Computing Fundamentals

[c4.drawio.xml](c4.drawio.xml) is a Draw.io drawing. It can be opened at https://app.diagrams.net/ or in Draw.io Desktop.

## Setup

Create the ccfadmin account in AWS IAM: Perms limited to create accounts

Create the CCF-Admins group in AWS IAM: Perms as described in Unit 01 README under "An **IAM Group** with all the needed permissions"

Create the ccf.sofree.us domain in AWS Route 53

Create appropriate zone-delegation in DNS

Build manage.ccf.sofree.us and add it to DNS
- Add ccfadmin with same password as the IAM ccfadmin
- Add add-learer script in /usr/local/bin/
- Add ccfadmin-sudo script in /etc/sudoers.d/

## Teardown

Here are some commands that might help accelerate the cleanup of RDS.

```bash
aws rds describe-db-clusters --region us-west-2 > dbc.json
jq '.DBClusters[].DBClusterIdentifier' < dbc.json

aws rds describe-db-instances --region us-west-2 > dbi.json
jq '.DBInstances[].DBInstanceIdentifier' < dbi.json

jq '.DBInstances[].DBInstanceIdentifier' < dbi.json | tr -d \" | while read DBI; do aws rds delete-db-instance --db-instance-identifier "$DBI" --region us-west-2 --skip-final-snapshot; done

jq '.DBClusters[].DBClusterIdentifier' < dbc.json | tr -d \" | while read DBC; do aws rds delete-db-cluster --db-cluster-identifier "$DBC" --region us-west-2 --skip-final-snapshot; done

aws rds describe-db-clusters --region us-west-2
```

Same, for LaunchTemplates.

```bash
aws ec2 describe-launch-templates --region us-west-2 > lt.json
jq .LaunchTemplates[].LaunchTemplateId lt.json 

aws ec2 delete-launch-template --region us-west-2 --launch-template-id $SOME_LT_ID
aws ec2 describe-launch-templates --region us-west-2 | jq
```
