# What is Pairing and Mobbing

Working together to solve a problem.

# Driver and Navigator

Old days the driver drove and the navigator sort of sat back and gave some instructions.

# Strong Style

Driver listens and follows instructions.

For an idea to go into the keyboard it must go through someone else's hands.

# We don't do it all the time

If you are sharing your screen you ARE THE DRIVER and LISTEN and FOLLOW INSTRUCTIONS

If you are NOT the driver you are PART OF THE MOB and are a type of NAVIGATOR

Navigate to the lowest level of abstraction or detail for the user:

You might say to your friend "click the close button"

You might have to say to your grandmother "in the upper right hand corner of the window is a button that looks like an X - that is the close button, single click it (just click it once) to close the window"
