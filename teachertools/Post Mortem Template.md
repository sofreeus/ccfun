# Post Mortem Template

## Title of Incident

### Incident number

## Authors / Written By

## Reporting Status

## Date

## Executive summary

### Brief Description

### Outage Description

### What happened

## Background

### Contributing Conditions

## Timeline

### Start Time

### End Time

### Chain of Events

### Duration

## Impact

### End User Impact and Users Affected

### Infrastructure Impact

### Productivity Impact

## Review

### What went well?

### What could have gone better?

### Where did we get lucky?

### What went wrong?

### Process Breakdowns

### Lessons Learned

## Root Causes

### Trigger / What caused the incident?

## Time to Detect - TTD

## Detection

## Time to Mitigate - TTM

## Mitigation

## Time to resolve - TTR

## Resolution

## Recommendations

## Action Items

### and follow ups

- item
- type (mitigate, prevent, process, other)
- who
- priority
- due date

## Open Questions

## Who was involved

## Supporting Documents
