# Design Document Template
*adapated from Tom Lemoncelli / The Practice of Cloud System Administration*

## Title

### Date

### Authors

#### Reviewers(s): (add reviewers, please link to their email addresses)

#### Approvers(s): (add approvers, please link to their email addresses)

##### Revision Number: Status: (draft, in review, approved, in progress)

### Executive Summary: (2–4 sentences explaining the project)

### Goals: (bullet list describing the problem being solved)

### Non-goals: (bullet list describing the limits of the project)

### Background: (terminology and history one needs to know to understand the design)

### High-Level Design: (a brief, high-level description of the design; diagrams are helpful)

### Detailed Design: (the design in detail; usually a top-down description)

### Alternatives Considered: (alternatives considered and rejected; include pros and cons of each)

### Security Concerns: (security/privacy ramifications and how those concerns are mitigated) 
